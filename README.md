<!--
 @Title:
 @@Copyright (c) 2023 by ${git_email} ITS Software LLC
 @Link: http://www.gx077.vip/
 @Author: akchen@job
 @Date: 2022-06-21 14:40:31
 @LastAuthor: akchen@job
 @LastTime: 2023-04-12 16:20:22
 @FilePath: /osx/resources/resources/git-resources/README.md
 @Description: gx077.vip
-->

[TOC]

# 资源信息

## 站点资源 web static `assets`

- images
- css
- javascript

## 文档 `document`

- markdown
- pdf
- word
- ppt

## 文件 `download` file

## 视频 `video`

- 短视频

## 声音 `music`

- 有声
- 音乐

## 说明

### 短网址

- [使用: dwz.win](http://dwz.wailian.work/)

### git 资源

> https://gitee.com/aklivecai/resources/raw/{分支名称}/{资源路径}

### 克隆分支

```sh
git clone -b master --single-branch git@gitee.com:aklivecai/resources.git base-clone
```
