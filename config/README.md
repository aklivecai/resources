<!--
 @Title:
 @@Copyright (c) 2023 by ${git_email} ITS Software LLC
 @Link: http://www.gx077.vip/
 @Author: akchen@job
 @Date: 2022-06-21 14:40:31
 @LastAuthor: akchen@job
 @LastTime: 2023-04-12 16:20:22
 @FilePath: /osx/resources/resources/git-resources/README.md
 @Description: gx077.vip
-->

[TOC]

# 配置 Config

> 这里是系统的配置文件，一般都是些基础配置信息
